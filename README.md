## prep mgmt cluster

```bash
kubectl apply -f management-cluster-config/kapp

```

## vsphere cluster with mission bundle 

```bash
imgpkg push \
-b harbor.proto.tsfrt.net/cluster-config/dev-cluster-1.0.0:vsphere \
-f cluster-templates/vsphere-cluster-template/ \
-f site1-vsphere-dev/cluster-values.yaml \
-f resourcesets/mission-controller \
-f resourcesets/common-apps
```
